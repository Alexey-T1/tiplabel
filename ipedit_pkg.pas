{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit ipedit_pkg;

{$warn 5023 off : no warning about unused units}
interface

uses
  ipedit, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('IPEdit', @IPEdit.Register);
end;

initialization
  RegisterPackage('ipedit_pkg', @Register);
end.
